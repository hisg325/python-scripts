#!/usr/bin/env python
import argparse
import boto3
import re

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--service", type=str)
parser.add_argument("-p", "--profile", type=str)
parser.add_argument("-r", "--region", type=str, default='eu-west-1')

args = parser.parse_args()

boto3.setup_default_session(profile_name=args.profile)

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html
ecs_client = boto3.client('ecs', region_name=args.region)

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs/client/list_task_definitions.html
list = ecs_client.list_task_definitions(familyPrefix=args.service)

for arn in list['taskDefinitionArns']:
    task = ecs_client.describe_task_definition(taskDefinition=arn)
    date_formatted = "?"
    if 'registeredAt' in task['taskDefinition']:
        date = task['taskDefinition']['registeredAt']
        date_formatted = date.isoformat()[:10]
    image = task['taskDefinition']['containerDefinitions'][0]['image']

    version = re.search(":.*$", image).group()[1:]
    print(date_formatted + ": " + version)
