# get-releases.py

Usage:
```
aws sso login --profile my-prof

python get-releases.py \
    --service my-service-v2-prod \
    --profile my-prof
```
